from flask import Flask, render_template,request, redirect
from flask_bootstrap import Bootstrap
import pickle

app = Flask(__name__)
Bootstrap(app)



@app.route('/')
def index():
	return render_template('index.html')

@app.route('/definitions')
def definitions():
	try:
		pickle_in = open('definitions.txt', 'rb')
		definitions = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		definitions = {}
	return render_template('definitions.html', definitions=definitions)



@app.route('/add_definition', methods=['POST'])
def add_definition():
	try:
		pickle_in = open('definitions.txt', 'rb')
		definitions = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		definitions = {}
	name = request.form['name']
	content = request.form['content']
	index = len(definitions)
	definitions[name] =  (index, content)
	pickle_out = open('definitions.txt','wb')
	pickle.dump(definitions,pickle_out)
	pickle_out.close()
	return redirect('definitions')

@app.route('/del_definition', methods=['POST'])
def delete_definition():
	pickle_in = open('definitions.txt', 'rb')
	definitions = pickle.load(pickle_in)
	pickle_in.close()	
	name = request.form['name']
	del definitions[name]
	pickle_out = open('definitions.txt','wb')
	pickle.dump(definitions,pickle_out)
	pickle_out.close()
	return redirect('definitions')	





@app.route('/theorems')
def theorems():
	try:
		pickle_in = open('theorems.txt', 'rb')
		theorems = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		theorems = {}
	indexes = ["a"+str(i) for i in range(len(theorems))]
	return render_template('theorems.html',theorems=theorems,indexes=indexes)

@app.route('/add_theorem', methods=['POST'])
def add_theorem():
	try:
		pickle_in = open('theorems.txt', 'rb')
		theorems = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		theorems = {}
	name = request.form['name']
	statement = request.form['content']
	proof = request.form['proof']
	index = len(theorems)
	theorems[name] = (index,statement,proof)
	pickle_out = open('theorems.txt','wb')
	pickle.dump(theorems,pickle_out)
	pickle_out.close()
	return redirect('theorems')


@app.route('/del_theorem', methods=['POST'])
def delete_theorem():
	pickle_in = open('theorems.txt', 'rb')
	theorems = pickle.load(pickle_in)
	pickle_in.close()
	name = request.form['name']
	del theorems[name]
	pickle_out = open('theorems.txt','wb')
	pickle.dump(theorems,pickle_out)
	pickle_out.close()
	return redirect('theorems')

@app.route('/edit_theorem', methods=['POST'])
def edit_theorem():
	pickle_in = open('theorems.txt', 'rb')
	theorems = pickle.load(pickle_in)
	pickle_in.close()
	name = request.form['name']
	index = theorems[name][0]
	newsolution = request.form['newsolution']
	newstatement = request.form['newstatement']
	theorems[name] = (index,newstatement,newsolution)
	pickle_out = open('theorems.txt','wb')
	pickle.dump(theorems,pickle_out)
	pickle_out.close()
	return redirect('theorems')







@app.route('/examples')
def examples():
	try:
		pickle_in = open('examples.txt','rb')
		examples = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		examples = {}
	indexes = ["a"+str(i) for i in range(len(examples))]
	return render_template('examples.html', examples=examples,indexes=indexes)


@app.route('/add_example', methods=['POST'])
def add_example():
	try:
		pickle_in = open('examples.txt', 'rb')
		examples = pickle.load(pickle_in)
		pickle_in.close()
	except FileNotFoundError:
		examples = {}
	name = request.form['name']
	content = request.form['content']
	solution = request.form['solution']
	index = len(examples)
	examples[name] =  (index, content, solution)
	pickle_out = open('examples.txt','wb')
	pickle.dump(examples,pickle_out)
	pickle_out.close()
	return redirect('examples')

@app.route('/del_example',methods=['POST'])
def delete_example():
	pickle_in = open('examples.txt', 'rb')
	examples = pickle.load(pickle_in)
	pickle_in.close()
	name = request.form['name']
	del examples[name]
	pickle_out = open('examples.txt','wb')
	pickle.dump(examples,pickle_out)
	pickle_out.close()
	return redirect('examples')


@app.route('/edit_example', methods=['POST'])
def edit_example():
	pickle_in = open('examples.txt', 'rb')
	examples = pickle.load(pickle_in)
	pickle_in.close()
	name = request.form['name']
	index = examples[name][0]
	newsolution = request.form['newsolution']
	newstatement = request.form['newstatement']
	examples[name] = (index,newstatement,newsolution)
	pickle_out = open('examples.txt','wb')
	pickle.dump(examples,pickle_out)
	pickle_out.close()
	return redirect('examples')


	
if __name__ == '__main__':
	app.run(debug=True)